package com.gopal151295.rob07008noteapp;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import static com.gopal151295.rob07008noteapp.NoteEditActivity.MSG;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {

    private static final int REQUEST_ADDNOTE = 1;
    private static final String TAG = "MainActivity";
    public static final String MSG_TO_EDIT = "MSG_TO_EDIT";
    private static final int REQUEST_EDITNOTE = 2;
    private static final String NOTES = "NOTES";
    private static final String MY = "MY";
    private static final String POSITION = "POSITION";
    private ListView listView;
    private ArrayList<String> notes;
    private ArrayAdapter<String> adapter;
    private String msgToSave;
    private int currentNoteNumber;
    private TinyDB tinyDb;
    private AlertDialog dialog;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (savedInstanceState != null){
            currentNoteNumber = savedInstanceState.getInt(POSITION);
        }

        tinyDb = new TinyDB(getApplicationContext());

        listView = (ListView) findViewById(R.id.acitivity_main_list_note);
        notes = new ArrayList<>();




        if (!tinyDb.getListString(NOTES).isEmpty()){
            notes.clear();
            notes = tinyDb.getListString(NOTES);
        }
        adapter = new ArrayAdapter<String>(getBaseContext(), R.layout.list_item, notes);
        listView.setAdapter(adapter);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.add);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, NoteEditActivity.class);
                startActivityForResult(intent, REQUEST_ADDNOTE);
            }
        });



        listView.setOnItemClickListener(this);
        listView.setOnItemLongClickListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        tinyDb.putListString(NOTES, notes);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.activity_main_menu_add) {
            Intent intent = new Intent(this, NoteEditActivity.class);
            startActivityForResult(intent, REQUEST_ADDNOTE);
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        msgToSave = data.getStringExtra(MSG);
        if (msgToSave.isEmpty())
            return;
        if (requestCode == REQUEST_ADDNOTE ){
            if (resultCode == RESULT_OK){
                notes.add(0, msgToSave);
                adapter.notifyDataSetChanged();
            }
        }else if(requestCode == REQUEST_EDITNOTE){
            if (resultCode == RESULT_OK){
                notes.remove(currentNoteNumber);
                notes.add(0, msgToSave);
                adapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        currentNoteNumber = position;
        Intent intent = new Intent(this, NoteEditActivity.class);
        String msgToEdit = notes.get(position);
        intent.putExtra(MSG_TO_EDIT, msgToEdit);
        startActivityForResult(intent, REQUEST_EDITNOTE);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
        currentNoteNumber = position;
        dialog = new AlertDialog.Builder(this)
                .setTitle("ARE YOU SURE?")
                .setMessage("Do you want to delete this note permanently?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setCancelable(false)
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        adapter.remove(adapter.getItem(currentNoteNumber));
                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setNeutralButton("CANCEL", null)
                .show();

        doKeepDialog(dialog);
        return true;
    }
//
    private static void doKeepDialog(Dialog dialog){
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(POSITION, currentNoteNumber);
    }
}
