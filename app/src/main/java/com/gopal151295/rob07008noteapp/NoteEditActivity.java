package com.gopal151295.rob07008noteapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

public class NoteEditActivity extends AppCompatActivity implements TextWatcher {
    public static final String MSG = "MSG";
    private static final String TAG = "NoteEditActivity";
    private static final String MY = "MY";
    private EditText msgEditText;
    private ActionBar actionBar;
    private String msg;
    private boolean isEdited;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_noteedit);

        actionBar= getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("Edit Note");
        }

        msgEditText = (EditText) findViewById(R.id.activity_noteedit_text);

        Intent intent = getIntent();
        msg = intent.getStringExtra(MainActivity.MSG_TO_EDIT);

        msgEditText.setText(msg);
        msgEditText.setSelection(msgEditText.getText().length());

        msgEditText.addTextChangedListener(this);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            saveTheNote();
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void saveTheNote() {
        msg = msgEditText.getText().toString();
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.putExtra(MSG, msg);
        if (isEdited)
            setResult(RESULT_OK, intent);
        else
            setResult(RESULT_CANCELED, intent);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        isEdited = true;
        msg = msgEditText.getText().toString();
    }

    @Override
    public void afterTextChanged(Editable s) {
    }
}
